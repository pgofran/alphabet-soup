# Alphabet Soup

> A Java project built with Maven that provides an answer key for word search puzzles.

## How to run

With Maven:
Change directories to the location of this project and run:
mvn compile exec:java -Dexec.mainClass="pjg.alphabet_soup.AlphabetSoup" -Dexec.args="./src/test/resources/test_1_simple.txt"

Note: If an argument is not specified then the default file will be used

To execute JUnit tests:
mvn clean install

## Requirements
Java compiler 1.8+